# Similaridade entre produtos


* There are two main programs in this application.
* The first one, from a products Json file, creates a new products file with an additional vector of feature tags
* The another program searches which the three products have the largest similarity with a given product.


### How do I get set up? ###

* Dependencies
To get the Dependencies, simply run the following command:

```sh
$ mvn clean package
```


# Unit Tests

* We are using JUnit for unit tests.

* To run, execute the following steps in terminal:

```sh
$ mvn test
```


To start the first program, you can run the following command:

```sh
mvn -q clean compile exec:java -Dexec.mainClass="com.produtos.usecases.GerarArquivoVetorTags" -Dexec.args="src/test/resources/produtos.txt"
```

To start the second program, you can run the following command:

```sh
mvn -q clean compile exec:java -Dexec.mainClass="com.produtos.usecases.BuscarProdutosSimilares" -Dexec.args="src/test/resources/produtosTagged.txt 7533"
```