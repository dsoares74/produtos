package com.produtos.domains;

import java.util.List;

public class ResultProductSimilarity {
    private final Product searchedProduct;
    private final List<Product> products;

    public ResultProductSimilarity(final Product searchedProduct, final List<Product> products) {
        this.searchedProduct = searchedProduct;
        this.products = products;
    }

    public Product getSearchedProduct() {
        return searchedProduct;
    }

    public List<Product> getProducts() {
        return products;
    }
}
