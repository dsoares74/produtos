package com.produtos.domains;

public enum Tag {
    NEUTRO,
    VELUDO,
    COURO,
    BASICS,
    FESTA,
    WORKWEAR,
    INVERNO,
    BOHO,
    ESTAMPAS,
    BALADA,
    COLORIDO,
    CASUAL,
    LISO,
    MODERNO,
    PASSEIO,
    METAL,
    VIAGEM,
    DELICADO,
    DESCOLADO,
    ELASTANO;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
