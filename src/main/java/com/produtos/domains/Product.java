package com.produtos.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Product {
    private Integer id;

    private String name;

    private Tag[] tags;

    private int[] tagsVector = new int[20];

    @JsonIgnore
    private Double similarity;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Tag[] getTags() {
        return tags;
    }

    public void setTags(final Tag[] tags) {
        this.tags = tags;
    }

    public int[] getTagsVector() {
        return tagsVector;
    }

    public void setTagsVector(final int[] tagsVector) {
        this.tagsVector = tagsVector;
    }

    public Double getSimilarity() {
        return similarity;
    }

    public void setSimilarity(final Double similarity) {
        this.similarity = similarity;
    }

}
