package com.produtos.usecases;

import com.produtos.domains.Product;
import com.produtos.domains.ResultProductSimilarity;

public class BuscarProdutosSimilares {
    private static final String MESSAGE1 = "Os tres produtos mais similares ao produto %d (%s) %nsao:%n";
    private static final String MESSAGE2 = "- %d (%s) com S=%1.2f%n";

    public static void main(String[] args) {
        checkArguments(args);

        final String filename = args[0];
        final Integer id = Integer.valueOf(args[1]);
        ProductsSimilarity similarity = new ProductsSimilarity();

        try {
            final ResultProductSimilarity resultProductSimilarity = similarity.findProductsSimilarities(id, filename);
            final Product product = resultProductSimilarity.getSearchedProduct();
            System.out.printf(MESSAGE1, product.getId(), product.getName());

            resultProductSimilarity.getProducts()
                    .forEach(prod -> System.out.printf(MESSAGE2, prod.getId(), prod.getName(), prod.getSimilarity()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void checkArguments(final String[] args) {
        if (args.length < 2) {
            System.out.println("Usage: java BuscarProdutosSimilares <nome do arquivo de produtos> <id do produto>");
            System.exit(0);
        }
    }

}
