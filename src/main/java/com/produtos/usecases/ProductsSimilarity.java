package com.produtos.usecases;

import com.produtos.domains.Product;
import com.produtos.domains.ResultProductSimilarity;
import com.produtos.exceptions.ProductNotFoundException;
import com.produtos.gateways.ReadProducts;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;

public class ProductsSimilarity {

    private ReadProducts readProducts;

    public ProductsSimilarity() {
        this.readProducts = new ReadProducts();
    }

    public ResultProductSimilarity findProductsSimilarities(final Integer id, final String filename) throws IOException {
        final Map<Integer, Product> idProductMap = loadIndexedProducts(filename);
        final Product product = findProduct(idProductMap, id);

        return new ResultProductSimilarity(product, findMostSimilarities(product, idProductMap.values()));
    }

    private Product findProduct(final Map<Integer, Product> indexedProducts, final Integer id) {
        return Optional.ofNullable(indexedProducts.remove(id))
                .orElseThrow(() -> new ProductNotFoundException("Product with specified ID not found in file"));
    }

    private Map<Integer, Product> loadIndexedProducts(String filename) throws IOException {
        final List<Product> products = readProducts.loadProducts(filename);
        return products.stream().collect(Collectors.toMap(Product::getId, identity()));
    }

    private List<Product> findMostSimilarities(Product p, Collection<Product> products) {
        return products.stream().peek(product ->
                product.setSimilarity(CalcEuclideanDistance.calcule(p.getTagsVector(), product.getTagsVector()))
        ).sorted((p1, p2) -> Double.compare(p2.getSimilarity(), p1.getSimilarity())).limit(3).collect(toList());
    }
}
