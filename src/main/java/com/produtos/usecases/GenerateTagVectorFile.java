package com.produtos.usecases;

import com.produtos.domains.Product;
import com.produtos.gateways.json.ProductsJson;
import com.produtos.gateways.WriteProducts;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

public class GenerateTagVectorFile {
    private static final String SUFFIX_NAME = "Tagged";
    private static final int DOT = 46;

    private final String source;
    private WriteProducts writeProducts;
    private String filename;

    public GenerateTagVectorFile(final String source) {
        this.source = source;
        this.writeProducts = new WriteProducts();
    }

    public void execute(final List<Product> products) throws IOException {
        final ProductsJson json = new ProductsJson();
        json.setProducts(products);
        writeProducts.execute(createNewFile(source), json);
    }

    private File createNewFile(final String source) {
        int dotIndex = source.lastIndexOf(DOT);
        final String extension = source.substring(dotIndex);
        this.filename = source.substring(0, dotIndex) + SUFFIX_NAME + extension;
        return Paths.get(this.filename).toFile();
    }

    public String getFilename() {
        return filename;
    }
}
