package com.produtos.usecases;

import java.util.List;

public class CalcEuclideanDistance {

    public static Double calcule(final int[] c1, final int[] c2) {
        double sum = 0;

        for (int i = 0; i < c1.length; i++) {
            sum += Math.pow(c1[i] - c2[i], 2);
        }

        return Math.sqrt(sum);
    }

}
