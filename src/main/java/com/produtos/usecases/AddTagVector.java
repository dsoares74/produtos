package com.produtos.usecases;

import com.produtos.domains.Product;
import com.produtos.gateways.ReadProducts;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddTagVector {
    private ReadProducts readProducts;

    public AddTagVector() {
        this.readProducts = new ReadProducts();
    }

    public List<Product> execute(final String filename) throws IOException {
        final List<Product> products = readProducts.loadProducts(filename);
        return products.stream()
                .peek(prod -> prod.setTagsVector(generateTagVector(prod))).collect(Collectors.toList());
    }

    private int[] generateTagVector(final Product product) {
        int[] vector = new int[20];
        Stream.of(product.getTags()).forEach(tag -> vector[tag.ordinal()] = 1);
        return vector;
    }
}
