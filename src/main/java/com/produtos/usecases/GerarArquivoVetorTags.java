package com.produtos.usecases;

import com.produtos.domains.Product;

import java.io.IOException;
import java.util.List;

public class GerarArquivoVetorTags {

    public static void main(String[] args) {
        checkArguments(args);
        final AddTagVector addTagVector = new AddTagVector();

        try {
            final List<Product> products = addTagVector.execute(args[0]);
            final GenerateTagVectorFile generator = new GenerateTagVectorFile(args[0]);
            generator.execute(products);

            System.out.printf("Arquivo %s gerado com sucesso%n", generator.getFilename());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void checkArguments(final String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: java GerarArquivoVetorTags <nome do arquivo de produtos>");
            System.exit(0);
        }
    }

}
