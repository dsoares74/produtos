package com.produtos.exceptions;

public class ProductNotFoundException extends RuntimeException {

    public ProductNotFoundException() {
        super();
    }

    public ProductNotFoundException(final String message) {
        super(message);
    }
}
