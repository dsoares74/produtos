package com.produtos.gateways;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.produtos.gateways.json.ProductsJson;

import java.io.File;
import java.io.IOException;

public class WriteProducts {

    public void execute(final File file, final ProductsJson json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
        mapper.writeValue(file, json);
    }
}
