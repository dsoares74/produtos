package com.produtos.gateways.json;

import com.produtos.domains.Product;

import java.util.List;

public class ProductsJson {
    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(final List<Product> products) {
        this.products = products;
    }
}
