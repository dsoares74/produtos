package com.produtos.gateways;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.produtos.domains.Product;
import com.produtos.gateways.json.ProductsJson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class ReadProducts {

    public List<Product> loadProducts(String filename) throws IOException {
        final List<Product> products;
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, true);

        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            final ProductsJson productsJson = mapper.readValue(reader, ProductsJson.class);

            products = productsJson.getProducts();
        }

        return products;
    }

}
