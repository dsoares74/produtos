package com.produtos.templates;

import java.util.Arrays;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.produtos.domains.Product;
import com.produtos.domains.Tag;

public class ProductTemplate implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(Product.class).addTemplate("valid1", new Rule() {{
            add("id", 7516);
            add("name", "VESTIDO WRAP FLEUR");
            add("tags", new Tag [] {Tag.NEUTRO, Tag.LISO, Tag.BASICS, Tag.VIAGEM});
            add("tagsVector", new int[] {1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0} );
        }});

        Fixture.of(Product.class).addTemplate("valid2", new Rule() {{
            add("id", 8104);
            add("name", "VESTIDO BABADO TURTLENECK");
            add("tags", new Tag[] {Tag.CASUAL, Tag.METAL, Tag.DELICADO, Tag.NEUTRO, Tag.BASICS, Tag.INVERNO, Tag.VIAGEM});
            add("tagsVector", new int[] {1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0});
        }});

        Fixture.of(Product.class).addTemplate("valid3", new Rule() {{
            add("id", 8080);
            add("name", "VESTIDO CURTO RENDA VISCOSE");
            add("tags", new Tag [] {Tag.NEUTRO, Tag.WORKWEAR, Tag.MODERNO, Tag.DESCOLADO, Tag.LISO, Tag.ELASTANO});
            add("tagsVector", new int[] {1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1});
        }});

        Fixture.of(Product.class).addTemplate("valid4", new Rule() {{
            add("id", 8291);
            add("name", "VESTIDO MANGA COMPRIDA COSTAS");
            add("tags", new Tag [] {Tag.INVERNO, Tag.ESTAMPAS, Tag.DELICADO, Tag.DESCOLADO, Tag.CASUAL, Tag.PASSEIO, Tag.BASICS});
            add("tagsVector", new int[] {0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0});
        }});

        Fixture.of(Product.class).addTemplate("valid5", new Rule() {{
            add("id", 8310);
            add("name", "VESTIDO CURTO PONTO ROMA MANGA");
            add("tags", new Tag [] {Tag.CASUAL, Tag.METAL, Tag.DELICADO, Tag.DESCOLADO, Tag.ELASTANO, Tag.ESTAMPAS});
            add("tagsVector", new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1});
        }});

        Fixture.of(Product.class).addTemplate("valid6", new Rule() {{
            add("id", 7613);
            add("name", "VESTIDO LONGO BABADO");
            add("tags", new Tag [] {Tag.CASUAL, Tag.LISO, Tag.PASSEIO, Tag.COLORIDO, Tag.BOHO});
            add("tagsVector", new int[] {1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0});
        }});
    }
}
