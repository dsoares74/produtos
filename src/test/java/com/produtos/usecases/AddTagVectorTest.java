package com.produtos.usecases;

import com.produtos.domains.Product;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Arrays;

import static org.junit.Assert.*;

public class AddTagVectorTest {
    private static final int TOTAL = 25;
    private AddTagVector addTagVector;

    @Before
    public void setUp() throws Exception {
        addTagVector = new AddTagVector();
    }

    @Test(expected = IOException.class)
    public void executeWithInexistentFile() throws Exception {
        addTagVector.execute("fake");
    }

    @Test
    public void execute() throws Exception {
        final List<Product> products = addTagVector.execute(getClass().getResource("/produtos.txt").getPath());
        assertNotNull(products);
        assertEquals(TOTAL, products.size());
        assertTrue(products.stream().allMatch(product -> Arrays.stream(product.getTagsVector()).anyMatch(tag -> tag == 1)));
    }

}