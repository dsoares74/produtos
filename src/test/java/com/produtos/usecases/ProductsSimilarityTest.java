package com.produtos.usecases;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.produtos.domains.Product;
import com.produtos.domains.ResultProductSimilarity;
import com.produtos.domains.Tag;
import com.produtos.exceptions.ProductNotFoundException;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class ProductsSimilarityTest {

    private ProductsSimilarity similarity;

    @Before
    public void setUp() throws Exception {
        similarity = new ProductsSimilarity();
    }

    @Test(expected = IOException.class)
    public void findProductsSimilaritiesWithInexistentFile() throws Exception {
        similarity.findProductsSimilarities(0, "fake");
    }

    @Test(expected = ProductNotFoundException.class)
    public void findProductsSimilaritiesWithInvalidID() throws Exception {
        similarity.findProductsSimilarities(0, getClass().getResource("/produtos.txt").getPath());
    }

    @Test
    public void findProductsSimilarities() throws Exception {
        final ResultProductSimilarity result =
                similarity.findProductsSimilarities(8104, getClass().getResource("/produtos.txt").getPath());
        assertNotNull(result);
        assertNotNull(result.getProducts());
        assertNotNull(result.getSearchedProduct());
        assertFalse(result.getProducts().isEmpty());
        assertEquals(8104, result.getSearchedProduct().getId().intValue());
    }

}