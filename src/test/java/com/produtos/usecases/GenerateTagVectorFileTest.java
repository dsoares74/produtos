package com.produtos.usecases;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.produtos.domains.Product;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

public class GenerateTagVectorFileTest {
    private GenerateTagVectorFile generateTagVectorFile;
    private Path path;

    @BeforeClass
    public static void load() {
        FixtureFactoryLoader.loadTemplates("com.produtos");
    }

    @Before
    public void setUp() throws Exception {
        path = Paths.get(getClass().getResource("/produtos.txt").getPath());
        generateTagVectorFile = new GenerateTagVectorFile(path.toAbsolutePath().toString());
    }

    @Test
    public void execute() throws Exception {
        final List<Product> list = Fixture.from(Product.class)
                .gimme(6, "valid1", "valid2", "valid3", "valid4", "valid5", "valid6");

        final Path newPath = Paths.get(this.path.getParent().toString() + "/produtosTagged.txt");
        Files.deleteIfExists(newPath);
        assertFalse(Files.exists(newPath));
        generateTagVectorFile.execute(list);
        assertTrue(Files.exists(newPath));
        Files.delete(newPath);
    }

}